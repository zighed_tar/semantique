// Lemmes nécessaires pour l'arithmétique non linéaire
//@ lemma distr_right: forall x, y, z. x*(y+z) == (x*y)+(x*z) ;
//@ lemma distr_left: forall x, y, z. (x+y)*z == (x*z)+(y*z) ;
//@	function int square (int x) = x * x;
int isqrt(int x) {
  // Le résultat doit être la partie entière de la racine carrée de x
  //@ requires x>=0 ;
  //@ ensures square(result) <= x && x < square(result+1);
  
  
  int count = 0;
  int sum = 1;
  while (sum <= x) { 
  	//@ variant x - square(count);
  	//@ invariant count >=0;
  	//@ invariant square(count+1) == sum;
  	//@ invariant square(count) <= x;
    count++;
    sum = sum + (2*count+1);
  }
  return count;
}   
