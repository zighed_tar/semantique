theory NikAxWeakestPrecondition

imports NikAxiomatic NikBigStepProperties

begin

text \<open>
  We gave the rules to derive Hoare triplets: \<turnstile> {P} instr {Q}.
  Is this definition sound and complete?
    - soundness: if a Hoare triplet is derivable, it is valid?
    - completeness: if a Hoare triplet is valid, is it derivable?
\<close>

text \<open>Soundness : if a Hoare triplet is derivable, it is valid\<close>
theorem axiomatic_sound:"\<turnstile> {P} instr {Q} \<Longrightarrow> \<Turnstile> {P} instr {Q}"
proof(induction rule: hoare_sem.induct)
  case (Nop P) 
  then show ?case using hoare_valid_def by auto
next
  case (VDecl P v)
  then show ?case using hoare_valid_def by auto
next
  case (Assign P u v)
  then show ?case using hoare_valid_def by auto
next
  case (Seq P i1 Q i2 R)
  then show ?case using hoare_valid_def by auto
next
  case (If P c iT Q iF)
then show ?case using hoare_valid_def by auto
next
  case (While P c body) \<comment> \<open>This is the only difficult case\<close>
  { fix s t \<comment> \<open>soit s et t deux états de la machine\<close>
    have \<open>\<lbrakk> (while c do body done, s) \<leadsto> t; P s\<rbrakk> \<Longrightarrow> P t \<and> \<not>evalbool c t\<close>
    proof (induction "while c do body done" s t rule: big_step_induct)
      case WhileFalse thus ?case by simp 
    next 
      case WhileTrue thus ?case using While.IH hoare_valid_def by auto
    qed
  }
  then show ?case using hoare_valid_def by blast
next
  case (Conseq P' P instr Q Q')
  then show ?case using hoare_valid_def by auto
\<comment> \<open>On peut factoriser si on veut se concentrer sur le cas While: qed (auto simp add:hoare_valid_def)\<close>
qed

text \<open>
  Completeness : every valid Hoare triplet is derivable
  For this, we need to introduce the notion of weakest precondition.
  For a given instruction and postcondition, this is the precondition
  which makes the postcondition hold on any state that can be reached
  by executing the instruction.
\<close>
text \<open>
  Weakest precondition for validating postcondition Q after executing instruction instr
\<close>
definition weakestpre :: "instruction \<Rightarrow> assertion \<Rightarrow> assertion"
where "weakestpre instr Q = (\<lambda>s. \<forall>t. (instr, s) \<leadsto> t \<longrightarrow> Q t)"

text \<open>Find the weakest precondition for each instruction\<close>
lemma wp_Nop[simp]: "weakestpre Nop Q = Q"
unfolding weakestpre_def by blast

lemma wp_Decl[simp]: "weakestpre (var x) Q = Q[with (N 0) for x]"
unfolding weakestpre_def by (rule ext, auto)

lemma wp_Assign[simp]: "weakestpre (x <- a) Q = Q[with a for x]"
unfolding weakestpre_def by (rule ext, auto)

lemma wp_Seq[simp]: "weakestpre (c\<^sub>1;;c\<^sub>2) Q = weakestpre c\<^sub>1 (weakestpre c\<^sub>2 Q)"
unfolding weakestpre_def by blast

lemma wp_If[simp]:
 "weakestpre (if cond then iT else iF fi) Q =
 (\<lambda>s. if evalbool cond s then weakestpre iT Q s else weakestpre iF Q s)"
unfolding weakestpre_def by force

text \<open>Show that the weakest precondition does not change when unfolding a while loop\<close>
lemma wp_While_unfold:
 "weakestpre (while cond do body done) Q s =
  weakestpre (if cond then body;; while cond do body done else Nop fi) Q s"
unfolding weakestpre_def
  using unfold_while by auto

lemma wp_While_True[simp]:
  assumes \<open>evalbool cond s\<close>
  shows
     \<open>weakestpre (while cond do body done) Q s
    = weakestpre (body;; while cond do body done) Q s\<close>
using assms by(simp add: wp_While_unfold)

lemma wp_While_False[simp]:
  assumes \<open>\<not> evalbool cond s\<close>
  shows
    \<open>weakestpre (while cond do body done) Q s = Q s\<close>
using assms wp_While_unfold by simp

text \<open>Show that the weakest precondition is a valid precondition\<close>
lemma weakestpre_is_pre: "\<turnstile> {weakestpre instr Q} instr {Q}"
proof (induction instr arbitrary: Q)
  case Nop
  then show ?case by  simp
next
  case (VarDecl x)
  then show ?case using hoare_sem.VDecl wp_Decl by presburger
next
  case (Assign x1 x2a)
  then show ?case by (simp add: Assign')
next
  case (Seq instr1 instr2)
  then show ?case by force
next
  case (Alternative x1 instr1 instr2)
  then show ?case by (auto intro: Conseq)
next
  case (While cond body)
  let ?w = "while cond do body done"                                
  show "\<turnstile> {weakestpre ?w Q} ?w {Q}" 
  proof (rule While') print_facts
    show "\<turnstile> {\<lambda>s. weakestpre ?w Q s \<and> evalbool cond s} body {weakestpre ?w Q}"
      by (rule strengthen_pre[OF _ While.IH] , fastforce)
    show \<open>\<forall>s. weakestpre ?w Q s \<and> \<not>evalbool cond s  \<longrightarrow> Q s\<close> by fastforce
  qed
qed

text \<open>
  We can now show that the axiomatic semantics of Niklaus is complete with regard 
  to the validity of Hoare triplets (defined in terms of big step semantics)
\<close>

\<comment> \<open>On peut l'écrire : theorem \<open>\<Turnstile> {P} instr {Q} \<Longrightarrow> \<turnstile> {P} instr {Q}\<close>\<close>
theorem axiomatic_complete:
  assumes "\<Turnstile> {P} instr {Q}"
  shows   "\<turnstile> {P} instr {Q}"
proof (rule strengthen_pre)
  show "\<forall>s. P s \<longrightarrow> weakestpre instr Q s"
    using hoare_valid_def weakestpre_def assms by fastforce
  show "\<turnstile> {weakestpre instr Q} instr {Q}" by (rule weakestpre_is_pre)
qed

text \<open>So the axiomatic semantics is sound and complete\<close>
corollary axiomatic_sound_complete: "\<turnstile> {P} instr {Q} \<longleftrightarrow> \<Turnstile> {P} instr {Q}"
using axiomatic_sound and axiomatic_complete by auto

end