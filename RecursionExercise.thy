theory RecursionExercise
imports Main
begin

text \<open>Define the factorial\<close>
fun f :: "nat => nat"
where
   "f 0 = 1"
 | "f (Suc n) = (Suc n) * f  n"
\<comment> \<open>fonction complete\<close>
text \<open>Some checks\<close>
value "f 0"
value "f 1"
value "f 5"

thm nat.split
thm nat.case
text \<open>Define the functor whose fixed point is the factorial\<close>
definition Fact :: "(nat => nat) => (nat => nat)"
where
   "Fact u \<equiv> (\<lambda>n. (case n of 
                    0 \<Rightarrow> 1
                  | Suc n' \<Rightarrow> n * (u n')) 
         )"
\<comment> \<open>On peut utiliser if and else : if n = 0 then 1 else n* (u (n-1)) et ça marche car n
est un nat
\<close>
\<comment> \<open>Test : \<close>
value "(5::nat) - 2"  \<comment> \<open>ça vaut 3\<close>

text \<open>
  Check what happens with successive applications 
  of the functor to the null function.
  ^^ is the composition power: f^^n = f \<circ> f \<circ> ... \<circ> f n times
\<close>
 \<comment> \<open>"on peut utiliser undefined on obtien  (fact^^x) undef)x+1"
avec : abbreviation  undef \<equiv> (\<lambda>n. undefined)\<close>
value "Fact (\<lambda> x. 0)"
value "(Fact (\<lambda> x. 0)) 0" \<comment> \<open>"1"\<close>
value "(Fact (Fact (\<lambda> x. 0))) 1" \<comment> \<open>"ça marche pas pour 1 cas, (\<lambda>x.0) n'est pas la fonction"\<close>
value "((Fact^^2) (\<lambda> x. 0)) 1" \<comment> \<open>"ça marche = 1_"\<close>
value "((Fact^^3) (\<lambda> x. 0)) 2" 
value "((Fact^^4) (\<lambda> x. 0)) 3"
value "((Fact^^5) (\<lambda> x. 0)) 4"
value "((Fact^^6) (\<lambda> x. 0)) 5" \<comment> \<open>"si on essaye avec 5 ça marche pas_"\<close>
\<comment> \<open>A noter : on part d'une fonction qui vaut 0 partout, on peut utiliser underfined à la place \<close>

thm ext
thm nat.split
text \<open>Show that f is a fixed point of Fact\<close>
lemma "Fact f = f"
  \<comment> \<open>  by (unfold Fact_def, rule ext, simp split: nat.split)\<close>
  apply (unfold Fact_def)
  apply (rule ext)
  apply ( split nat.split) \<comment> \<open> \<close>
  apply simp
  done

text \<open>Non constructive definition of the factorial\<close>
function g ::"nat \<Rightarrow> nat"
where
  "g 0 = 1"
| "g (Suc n) = (g (Suc (Suc n))) div (Suc n)"
  using not0_implies_Suc apply fastforce
    apply simp 
   apply blast
  by simp
termination sorry


text \<open>Define the matching functor\<close>
definition G :: "(nat => nat) => (nat => nat)"
where
   "G u \<equiv> (\<lambda>n. (case n of
              0 \<Rightarrow> 1
              | Suc n' \<Rightarrow> (u (Suc n) div (Suc n))))"


text \<open>
  Check what happens with successive applications 
  of the functor to the null function.
\<close>
value "(G (\<lambda>n. 0)) 0"
value "((G^^10) (\<lambda>n. 0)) 1" (* Cannot build g *)

text \<open>
  The factorial f is also a fixed point of G,
  but we cannot use G^^i to approximate it.
\<close>
thm g.induct
lemma "G f = f"
proof (unfold G_def, rule ext)
  fix n 
  show \<open>(case n of 0 \<Rightarrow> 1 | Suc n' \<Rightarrow> f (Suc n) div Suc n) = f n\<close>
  proof(cases n)
    case 0
    then show ?thesis by simp
  next
    case (Suc n')
    have \<open>f (Suc n) div (Suc n) = (Suc n) * (f n) div (Suc n)\<close> by simp
    also have \<open>... = f n\<close>
     using nonzero_mult_div_cancel_left by blast
    then show ?thesis using Suc by simp
  qed
qed

text \<open>
fun h :: \<open>nat \<Rightarrow> nat\<close>
  where 
\<open>h Suc 0 = 1\<close>
| \<open>h (Suc^^(2*(Suc k)) (0::nat)) = h ((Suc^^(2*k)) (0::nat))\<close>
| \<open>h n = h Suc (3*n)\<close>
\<close>

text \<open>Conjecture de Syracuse\<close>
function h :: "nat \<Rightarrow> nat"
where
  "h n = (
    if n < 2 then 1 
    else if n mod 2 = 0 then h (n div 2)
    else h (Suc (3*n)))"
  by simp+
termination sorry 

text \<open>Some checks...\<close>
value "h 0" \<comment> \<open>ne marche pas car la fonction ne termine pas si on on met n = 1, alors que la on inclue le cas 0 avec 1\<close>
value "h 2"
value "h (1+1)"
value "h 3"

text \<open>Definition of the matching functor\<close>
definition Syr :: "(nat => nat) => (nat => nat)"
where
  "Syr u \<equiv> (\<lambda>n. if n < 2 then 1 
                else if n mod 2 = 0 then u (n div 2)
                else u (Suc (3*n))
            )"

text \<open>Find the right power P to be able to terminate each call\<close>
\<comment> \<open>On utilisant undef on peut remplacer (\<lambda>x. 0) par undef\<close>
value "Syr (\<lambda>x. 0) 0"
value "Syr (\<lambda>x. 0) 1"
value "(Syr^^2) (\<lambda>x. 0) 2"
value "(Syr^^2) (\<lambda>x. 0) 3" \<comment> \<open>on voit pas l'erreur cas on utilise pas undef\<close>
value "(Syr^^8) (\<lambda>x. 0) 3" \<comment> \<open>3 10 5 16 8 4 2 1 \<close>
value "(Syr^^3) (\<lambda>x. 0) 4" \<comment> \<open>4 2 1\<close>
value "(Syr^^9) (\<lambda>x. 0) 6" \<comment> \<open>6 3 10 5 16 8 4 2 1\<close>

text \<open>Show that \<lambda>n. 1 a fixed point of Syr\<close>
lemma "Syr (\<lambda>n. 1) = (\<lambda>n. 1)"
  by (unfold Syr_def, rule ext, simp split:nat.split)

end
