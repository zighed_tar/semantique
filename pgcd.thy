theory pgcd
  imports Main
begin
	definition \<open>divides (y::int) (x::int) \<equiv>
	  \<exists>m. x = (m * y)
	\<close>

	definition \<open>is_m_gcd (x::int) (y::int) (d::int) \<equiv>
	  \<exists>u v. (((x * u) - (y * v)) = d) \<or> (((y * v) - (x * u)) = d)
	\<close>

	definition \<open>is_gcd (x::int) (y::int) (d::int) \<equiv>
	  (divides d x) \<and> ((divides d y) \<and> (is_m_gcd x y d))
	\<close>

	lemma gcd_1: \<open>
	  (x \<ge> y )\<Longrightarrow> (is_gcd x y d) = (is_gcd (x - y) y d)
	\<close> 
	proof -
    { fix x y d::int  assume H1: \<open>x\<ge>y\<close> and H2: "is_gcd (x -y) y d"
      show "is_gcd x y d"
      proof - 
        from H2 is_m_gcd[OF H1] have "devides d x" unfolding is_gcd_def by simp
      proof -
      
    }

	(*
	  int pgcd (int a, int b) {
	    //@ ensures is_gcd old(a) old(b) result ;
	    while (a != b) {
	      //@ variant a + b;
	      //@ invariant (a > 0) \<and> (b > 0);
	      if (a < b) {
	          b -= a;
	      } else {
	          a -= b;
	      }
	    }
	    //@ check a = b ;
	    return a;
	  }
	*)

end
