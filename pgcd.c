// Ajoutez ici les prédicats et lemmes nécessaires
//@ predicate divides(int y, int x) = exists m. x == m*y;
//@ predicate is_m_gcd(int x, int y, int d) = exists u ,v. (x*u - y*v == d) || (y*v - x*u == d);
//@ predicate is_gcd(int x, int y, int d) = divides(d,x) && divides(d,y) && is_m_gcd(x,y,d);
//@ lemma gcd_1: forall x, y, d. is_gcd(x,y,d) -> is_gcd(x-y,y,d); 

int pgcd(int a, int b) {
  // Le résultat doit être le PGCD de a et b
  //@ ensures is_gcd(old(a),old(b),result);
  //@ label s;
  while (a != b) {
  	//@ variant a+b;
  	//@ invariant a > 0 && b > 0;
  	//@ invariant forall d. is_gcd(at(a,s), at(b,s),d) <-> is_gcd(a,b,d);
    if (a < b) {
    
      b -= a;
    } else {
      a -= b;
    }
  }
  //@ check a==b;
  return a;
}
